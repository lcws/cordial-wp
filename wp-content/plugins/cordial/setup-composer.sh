#!/bin/sh

 #set this to executable and run this in the docker terminal to setup composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php --quiet
RESULT=$?
rm composer-setup.php
exit $RESULT