=== Plugin Name ===
Contributors: lostcoastweb
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=GMKPHAT7ZQKMQ&currency_code=USD&source=url
Tags: comments, spam, cordial
Requires at least: 5.2.3
Tested up to: 5.2.3
Requires PHP: 5.6
Stable tag: 0.2.0
License: Apache 2.0
License URI: http://www.apache.org/licenses/
 
Cordial uses machine learning to automatically moderate your Wordpress comments.  This is a BETA release of the Cordial.  
 
== Description ==
 
Cordial uses machine learning to automatically moderate your Wordpress comments.  This is a BETA release of the Cordial.  

## How it works 
When enabled, the Cordial Wordpress plugin will send all comments to the cordial server for analysis.  Each comment receives a score from 0-100 with a score of 0 being probably benign and 100 being probably offensive.  Depending on your site settings (see Dashboard -> Settings -> Cordial) comments meeting specific thresholds can be flagged as "pending" or "trash". 

## Examples
Below are some sample comments and their related Cordial score:
* (97) "This site sucks!"
* (91) "I hate you"
* (33) "Why are you being to terrible?"
* (19) "You don't make any sense"
* (12) "Your response makes no sense"

## Disclaimer
Cordial is being actively developed by a small team and is in early beta.  We make no guarantee that Cordial work work as intended.  Expect bugs.  Things might break.  If they do, we would appreciate your help in resolving issues. Please leave feedback at https://gitlab.com/lcws/cordial-wp
 
== Installation ==
 
1. Upload the zipped folder to your plugins directory or install automatically through the Wordpress admin panel.
2. Activate the plugin through the 'Plugins' menu in WordPress
3. From the admin dashboard, visit Settings -> Cordial to receive a Cordial API key and set thresholds for your site.
 
== Changelog ==

= 0.2.0 =
* Cordial score now shows up as a column in the "Comments" page on the WP Admin dashboard.

= 0.1.2 =
* UX enhancements to registration form.

= 0.1.0 =
* Initial public release
 
== Upgrade Notice ==
 
 
 